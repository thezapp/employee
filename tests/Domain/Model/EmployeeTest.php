<?php

namespace Employee\Tests\Domain\Model;

use Employee\Domain\Event\Employee\EmployeeActivated;
use Employee\Domain\Event\Employee\EmployeeAddressChanged;
use Employee\Domain\Event\Employee\EmployeeArchived;
use Employee\Domain\Event\Employee\EmployeeCreated;
use Employee\Domain\Event\Employee\EmployeePhoneAdded;
use Employee\Domain\Event\Employee\EmployeePhoneRemoved;
use Employee\Domain\Event\Employee\EmployeeRenamed;
use Employee\Domain\Model\Address;
use Employee\Domain\Model\Employee;
use Employee\Domain\Model\Id;
use Employee\Domain\Model\Name;
use Employee\Domain\Model\Phone;
use Employee\Domain\Model\Status;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    public function testCreate()
    {
        $employee = $this->createEmployeeWithParams(
            $id = new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            $name = new Name('lastname', 'firstName', 'middleName'),
            $address = new Address('country', 'city'),
            [$phone = new Phone(8, 923, 55500000)],
            $status = Status::active()
        );

        $this->assertTrue($employee->getId()->equalTo($id));
        $this->assertTrue($employee->getName()->equalTo($name));
        $this->assertTrue($employee->getAddress()->equalTo($address));
        $this->assertTrue($employee->getPhones()[0]->equalTo($phone));
        $this->assertTrue($employee->getStatuses()->first()->equalTo($status));

        $events = $employee->releaseEvents();

        $this->assertInstanceOf(EmployeeCreated::class, end($events));
    }

    public function testCreateExpectEmployeeMustHaveAtLeastOnePhoneNumberException()
    {
        $this->expectExceptionMessage('Employee must have at least one phone number');
        $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [],
            Status::active()
        );
    }

    public function testCreateWithEmptyFirstName()
    {
        $this->expectExceptionMessage('Passed value cannot be empty');
        $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', '', 'middleName'),
            new Address('country', 'city'),
            [new Phone(8, 923, 55500000)],
            Status::active()
        );
    }

    public function testCreateWithEmptyCountry()
    {
        $this->expectExceptionMessage('Passed value cannot be empty');
        $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('', 'city'),
            [new Phone(8, 923, 55500000)],
            Status::active()
        );
    }

    public function testArchive()
    {
        $employee = $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [new Phone(8, 923, 55500000)],
            Status::active()
        );

        $employee->releaseEvents();

        $employee->archive();

        $this->assertTrue($employee->getStatuses()->first()->equalTo(Status::archive()));

        $this->assertContainsOnlyInstancesOf(EmployeeArchived::class, $employee->releaseEvents());
    }

    public function testArchiveExpectAlreadyArchivedException()
    {
        $employee = $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [new Phone(8, 923, 55500000)],
            Status::archive()
        );

        $this->expectExceptionMessage('Employee already archived');
        $employee->archive();
    }

    public function testActivate()
    {
        $employee = $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [new Phone(8, 923, 55500000)],
            Status::archive()
        );

        $employee->releaseEvents();

        $employee->activate();

        $this->assertTrue($employee->getStatuses()->first()->equalTo(Status::active()));

        $this->assertContainsOnlyInstancesOf(EmployeeActivated::class, $employee->releaseEvents());
    }

    public function testActivateExpectAlreadyActivatedException()
    {
        $employee = $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [new Phone(8, 923, 55500000)],
            Status::active()
        );

        $this->expectExceptionMessage('Employee already activated');
        $employee->activate();
    }

    public function testRename()
    {
        $employee = $this->createEmployee();
        $employee->releaseEvents();

        $name = new Name('newLastName', 'newFirstName', 'newMiddleName');

        $employee->rename($name);

        $this->assertTrue($employee->getName()->equalTo($name));

        $this->assertContainsOnlyInstancesOf(EmployeeRenamed::class, $employee->releaseEvents());
    }

    public function testChangeAddress()
    {
        $employee = $this->createEmployee();
        $employee->releaseEvents();

        $address = new Address('newCountry', 'newCity', 'newRegion');

        $employee->changeAddress($address);

        $this->assertTrue($employee->getAddress()->equalTo($address));

        $this->assertContainsOnlyInstancesOf(EmployeeAddressChanged::class, $employee->releaseEvents());
    }

    public function testAddPhone()
    {
        $employee = $this->createEmployee();
        $employee->releaseEvents();

        $phone = new Phone(8, 923, 5551111);

        $employee->addPhone($phone);

        $this->assertTrue($employee->getPhones()[1]->equalTo($phone));

        $this->assertContainsOnlyInstancesOf(EmployeePhoneAdded::class, $employee->releaseEvents());
    }

    public function testAddPhoneExpectPhoneAlreadyAddedException()
    {
        $employee = $this->createEmployee();
        $employee->releaseEvents();

        $phone = new Phone(8, 923, 5551111);
        $employee->addPhone($phone);

        $this->expectExceptionMessage('Phone 89235551111 already added');
        $employee->addPhone($phone);
    }

    public function testRemovePhone()
    {
        $employee = $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [$phone = new Phone(8, 923, 55500000)],
            Status::active()
        );
        $employee->addPhone($phone1 = new Phone(8, 923, 5551111));
        $employee->releaseEvents();

        $employee->removePhone($phone);
        $employee->removePhone($phone1);

        $this->assertEmpty($employee->getPhones());

        $this->assertContainsOnlyInstancesOf(EmployeePhoneRemoved::class, $employee->releaseEvents());
    }

    private function createEmployee(): Employee
    {
        return $this->createEmployeeWithParams(
            new Id('702785fe-73d0-4a9a-9448-9a4bdc042df6'),
            new Name('lastname', 'firstName', 'middleName'),
            new Address('country', 'city'),
            [new Phone(8, 923, 5550000)],
            Status::active()
        );
    }

    /**
     * @param Phone[] $phones
     */
    private function createEmployeeWithParams(
        Id $id,
        Name $name,
        Address $address,
        array $phones,
        Status $status
    ): Employee {
        return new Employee(
            $id,
            $name,
            $address,
            $phones,
            $status
        );
    }
}
