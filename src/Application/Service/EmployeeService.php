<?php

declare(strict_types=1);

namespace Employee\Application\Service;

use Employee\Application\DTO\Employee as UIEmployee;
use Employee\Application\Factory\EmployeeFactory;
use Employee\Domain\Component\Dispatcher\EventDispatcherInterface;
use Employee\Domain\Component\IdGeneratorInterface;
use Employee\Domain\Event\Employee\EmployeeRemoved;
use Employee\Domain\Model\Address;
use Employee\Domain\Model\Employee;
use Employee\Domain\Model\EmployeeRepositoryInterface;
use Employee\Domain\Model\Id;
use Employee\Domain\Model\Name;
use Employee\Domain\Model\Phone;
use Employee\Domain\Model\Status;

class EmployeeService
{
    private EmployeeRepositoryInterface $employeeRepository;
    private EmployeeFactory $employeeFactory;
    private EventDispatcherInterface $eventDispatcher;
    private IdGeneratorInterface $idGenerator;

    public function __construct(
        EmployeeRepositoryInterface $employeeRepository,
        EmployeeFactory $employeeFactory,
        EventDispatcherInterface $eventDispatcher,
        IdGeneratorInterface $idGenerator
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->employeeFactory = $employeeFactory;
        $this->eventDispatcher = $eventDispatcher;
        $this->idGenerator = $idGenerator;
    }

    /**
     * @return UIEmployee[]
     */
    public function getAll(): array
    {
        $employees = $this->employeeRepository->getAll();

        return $this->employeeFactory->makeUIEmployees($employees);
    }

    public function getOne(Id $id): UIEmployee
    {
        $employee = $this->employeeRepository->getOne($id);

        return $this->employeeFactory->makeUIEmployee($employee);
    }

    public function create(Name $name, Address $address, array $phones, Status $status): UIEmployee
    {
        $employee = new Employee(
            new Id($this->idGenerator->generate()),
            $name,
            $address,
            $phones,
            $status
        );

        $this->employeeRepository->add($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());

        return $this->employeeFactory->makeUIEmployee($employee);
    }

    public function remove(Id $id): void
    {
        $employee = $this->employeeRepository->getOne($id);
        $this->employeeRepository->remove($employee);

        $this->eventDispatcher->dispatchEvent(new EmployeeRemoved($id));
    }

    public function archive(Id $id): void
    {
        $employee = $this->employeeRepository->getOne($id);

        $employee->archive();

        $this->employeeRepository->update($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());
    }

    public function activate(Id $id): void
    {
        $employee = $this->employeeRepository->getOne($id);

        $employee->activate();

        $this->employeeRepository->update($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());
    }

    public function rename(Id $id, Name $name): void
    {
        $employee = $this->employeeRepository->getOne($id);

        $employee->rename($name);

        $this->employeeRepository->update($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());
    }

    public function changeAddress(Id $id, Address $address): void
    {
        $employee = $this->employeeRepository->getOne($id);

        $employee->changeAddress($address);

        $this->employeeRepository->update($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());
    }

    public function addPhone(Id $id, Phone $phone): void
    {
        $employee = $this->employeeRepository->getOne($id);

        $employee->addPhone($phone);

        $this->employeeRepository->update($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());
    }

    public function removePhone(Id $id, Phone $phone): void
    {
        $employee = $this->employeeRepository->getOne($id);

        $employee->removePhone($phone);

        $this->employeeRepository->update($employee);

        $this->eventDispatcher->dispatchEvents($employee->releaseEvents());
    }
}
