<?php

declare(strict_types=1);

namespace Employee\Application\Factory;

use Employee\Application\DTO\Employee as UIEmployee;
use Employee\Domain\Model\Employee as DomainEmployee;

class EmployeeFactory
{
    /**
     * @param DomainEmployee[] $domainEmployees
     *
     * @return UIEmployee[]
     */
    public function makeUIEmployees(array $domainEmployees): array
    {
        $result = [];
        foreach ($domainEmployees as $domainEmployee) {
            $result[] = $this->makeUIEmployee($domainEmployee);
        }

        return $result;
    }

    public function makeUIEmployee(DomainEmployee $domainEmployee): UIEmployee
    {
        return new UIEmployee(
            $domainEmployee->getId()->toString(),
            $domainEmployee->getName()->toString(),
            $domainEmployee->getAddress()->toString(),
            $domainEmployee->getStatuses()->last()->toString(),
            array_map(function ($domainPhone) {
                return $domainPhone->toString();
            }, $domainEmployee->getPhones())
        );
    }
}
