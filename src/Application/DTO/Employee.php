<?php

declare(strict_types=1);

namespace Employee\Application\DTO;

class Employee
{
    private string $id;
    private string $name;
    private string $address;
    private string $status;

    /**
     * @var string[]
     */
    private array $phones;

    /**
     * @param string[] $phones
     */
    public function __construct(string $id, string $name, string $address, string $status, array $phones)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->status = $status;
        $this->phones = $phones;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string[]
     */
    public function getPhones(): array
    {
        return $this->phones;
    }
}
