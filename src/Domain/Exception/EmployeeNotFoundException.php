<?php

declare(strict_types=1);

namespace Employee\Domain\Exception;

use Employee\Domain\Model\Id;
use Throwable;

class EmployeeNotFoundException extends \Exception
{
    public function __construct(Id $employeeId, Throwable $previous = null)
    {
        parent::__construct($employeeId->toString(), 0, $previous);
    }
}
