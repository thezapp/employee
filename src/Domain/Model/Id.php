<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

class Id
{
    private string $value;

    private const NIL = '00000000-0000-0000-0000-000000000000';
    private const VALID_PATTERN = '\A[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}\z';

    public function __construct(string $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    private function validate(string $value): void
    {
        if (!$this->isValid($value)) {
            throw new \InvalidArgumentException("Passed value is not valid uuid: $value");
        }
    }

    private function isValid(string $uuid): bool
    {
        return $uuid === self::NIL || preg_match('/' . self::VALID_PATTERN . '/Dms', $uuid);
    }

    public function equalTo(Id $id): bool
    {
        return $this->value === $id->value;
    }

    public function toString(): string
    {
        return $this->value;
    }
}
