<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

class StatusCollection
{
    /**
     * @var Status[]
     */
    private array $statuses;

    /**
     * @param Status[] $statuses
     */
    public function __construct(array $statuses)
    {
        foreach ($statuses as $status) {
            $this->addStatus($status);
        }
    }

    public function first(): ?Status
    {
        return reset($this->statuses);
    }

    public function last(): ?Status
    {
        return end($this->statuses);
    }

    /**
     * @return Status[]
     */
    public function toArray(): array
    {
        return $this->statuses;
    }

    private function addStatus(Status $status): void
    {
        $this->statuses[] = $status;
    }
}
