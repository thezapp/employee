<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

class Name
{
    private string $lastName;
    private string $firstName;
    private ?string $middleName;

    public function __construct(string $lastName, string $firstName, string $middleName = null)
    {
        $this->validate($lastName);
        $this->validate($firstName);

        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
    }

    private function validate(string $value): void
    {
        if (strlen($value) === 0) {
            throw new \InvalidArgumentException("Passed value cannot be empty");
        }
    }

    public function equalTo(Name $name): bool
    {
        return $this->toString() === $name->toString();
    }

    public function toString(): string
    {
        return implode(' ', [
            $this->lastName,
            $this->firstName,
            $this->middleName
        ]);
    }
}
