<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

use Employee\Domain\Event\Employee\EmployeeActivated;
use Employee\Domain\Event\Employee\EmployeeAddressChanged;
use Employee\Domain\Event\Employee\EmployeeArchived;
use Employee\Domain\Event\Employee\EmployeeCreated;
use Employee\Domain\Event\Employee\EmployeePhoneAdded;
use Employee\Domain\Event\Employee\EmployeePhoneRemoved;
use Employee\Domain\Event\Employee\EmployeeRenamed;
use http\Exception\InvalidArgumentException;

class Employee
{
    private Id $id;
    private Name $name;
    private Address $address;

    /**
     * @var Phone[]
     */
    private array $phones = [];
    private StatusCollection $statuses;
    private \DateTimeImmutable $createdAt;
    private \DateTimeImmutable $updatedAt;

    private array $events = [];

    /**
     * @param Phone[] $phones
     */
    public function __construct(Id $id, Name $name, Address $address, array $phones, Status $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->addPhones($phones);
        $this->statuses = new StatusCollection([$status]);

        $this->createdAt = new \DateTimeImmutable();
        $this->refreshUpdatedAt();

        $this->events[] = new EmployeeCreated($this->id);
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function getPhones(): array
    {
        return $this->phones;
    }

    public function getStatuses(): StatusCollection
    {
        return $this->statuses;
    }

    public function archive()
    {
        $status = $this->statuses->last();

        if ($status->equalTo(Status::archive())) {
            throw new \DomainException('Employee already archived');
        }

        $this->statuses = new StatusCollection([Status::archive(), ...$this->statuses->toArray()]);

        $this->refreshUpdatedAt();

        $this->events[] = new EmployeeArchived($this->id);
    }

    public function activate()
    {
        $status = $this->statuses->last();

        if ($status->equalTo(Status::active())) {
            throw new \DomainException('Employee already activated');
        }

        $this->statuses = new StatusCollection([Status::active(), ...$this->statuses->toArray()]);

        $this->refreshUpdatedAt();

        $this->events[] = new EmployeeActivated($this->id);
    }

    public function rename(Name $name)
    {
        $this->name = $name;

        $this->refreshUpdatedAt();

        $this->events[] = new EmployeeRenamed($this->id);
    }

    public function changeAddress(Address $address)
    {
        $oldAddress = $this->address;

        $this->address = $address;

        $this->refreshUpdatedAt();

        $this->events[] = new EmployeeAddressChanged($this->id, $oldAddress, $address);
    }

    public function addPhone(Phone $inputPhone): void
    {
        foreach ($this->phones as $phone) {
            if ($phone->equalTo($inputPhone)) {
                throw new \DomainException("Phone {$inputPhone->toString()} already added");
            }
        }

        $this->phones[] = $inputPhone;

        $this->refreshUpdatedAt();

        $this->events[] = new EmployeePhoneAdded($this->id, $inputPhone);
    }

    public function removePhone(Phone $inputPhone): void
    {
        foreach ($this->phones as $key => $phone) {
            if ($phone->equalTo($inputPhone)) {
                unset($this->phones[$key]);
            }
        }

        $this->refreshUpdatedAt();

        $this->events[] = new EmployeePhoneRemoved($this->id, $inputPhone);
    }

    public function releaseEvents(): array
    {
        $events = $this->events;

        $this->events = [];

        return $events;
    }

    /**
     * @param Phone[] $phones
     */
    private function addPhones(array $phones)
    {
        if (count($phones) < 1) {
            throw new \InvalidArgumentException('Employee must have at least one phone number');
        }

        foreach ($phones as $phone) {
            $this->addPhone($phone);
        }
    }

    private function refreshUpdatedAt(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
