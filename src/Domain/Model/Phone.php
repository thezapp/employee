<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

class Phone
{
    private int $countryCode;
    private int $operatorCityCode;
    private int $number;

    public function __construct(int $countryCode, int $operatorCityCode, int $number)
    {
        $this->countryCode = $countryCode;
        $this->operatorCityCode = $operatorCityCode;
        $this->number = $number;
    }

    public function equalTo(Phone $phone): bool
    {
        return $this->toString() === $phone->toString();
    }

    public function toString(): string
    {
        return implode('', [
            $this->countryCode,
            $this->operatorCityCode,
            $this->number
        ]);
    }
}
