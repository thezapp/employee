<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

use Employee\Domain\Exception\EmployeeNotFoundException;

interface EmployeeRepositoryInterface
{
    /**
     * @return Employee[]
     */
    public function getAll(): array;

    /**
     * @throws EmployeeNotFoundException
     */
    public function getOne(Id $employeeId): ?Employee;

    public function add(Employee $employee): void;

    public function update(Employee $employee): void;

    public function remove(Employee $employee): void;
}
