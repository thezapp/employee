<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

class Status
{
    private const STATUS_ACTIVE = 'active';
    private const STATUS_ARCHIVE = 'archive';

    private const ALLOWABLE_TYPES = [
        self::STATUS_ACTIVE,
        self::STATUS_ARCHIVE,
    ];

    private string $status;
    private \DateTimeImmutable $createdAt;

    public function __construct(string $status)
    {
        if (!in_array($status, self::ALLOWABLE_TYPES, true)) {
            throw new \InvalidArgumentException("Unexpected value: {$status}");
        }

        $this->status = $status;
        $this->createdAt = new \DateTimeImmutable();
    }

    public static function active(): self
    {
        return new self(self::STATUS_ACTIVE);
    }

    public static function archive(): self
    {
        return new self(self::STATUS_ARCHIVE);
    }

    public function toString(): string
    {
        return $this->status;
    }

    public function equalTo(Status $status): bool
    {
        return $this->status === $status->status;
    }
}
