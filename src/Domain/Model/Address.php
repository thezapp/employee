<?php

declare(strict_types=1);

namespace Employee\Domain\Model;

class Address
{
    private string $country;
    private ?string $region;
    private string $city;
    private ?string $street;
    private ?string $house;

    public function __construct(
        string $country,
        string $city,
        string $region = null,
        string $street = null,
        string $house = null
    ) {
        $this->validate($country);
        $this->validate($city);

        $this->country = $country;
        $this->region = $region;
        $this->city = $city;
        $this->street = $street;
        $this->house = $house;
    }

    private function validate(string $value): void
    {
        if (strlen($value) === 0) {
            throw new \InvalidArgumentException("Passed value cannot be empty");
        }
    }

    public function equalTo(Address $address): bool
    {
        return $this->toString() === $address->toString();
    }

    public function toString(): string
    {
        return implode(', ', [
            $this->country,
            $this->region,
            $this->city,
            $this->street,
            $this->house
        ]);
    }
}
