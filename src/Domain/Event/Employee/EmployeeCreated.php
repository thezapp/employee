<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Id;

class EmployeeCreated implements EventInterface
{
    private Id $employeeId;
    private \DateTimeImmutable $createdAt;

    public function __construct(Id $employeeId)
    {
        $this->employeeId = $employeeId;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
