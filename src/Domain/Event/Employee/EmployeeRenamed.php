<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Id;

class EmployeeRenamed implements EventInterface
{
    private Id $employeeId;
    private \DateTimeImmutable $renamedAt;

    public function __construct(Id $employeeId)
    {
        $this->employeeId = $employeeId;
        $this->renamedAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getRenamedAt(): \DateTimeImmutable
    {
        return $this->renamedAt;
    }
}
