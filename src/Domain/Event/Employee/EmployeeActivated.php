<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Id;

class EmployeeActivated implements EventInterface
{
    private Id $employeeId;
    private \DateTimeImmutable $activatedAt;

    public function __construct(Id $employeeId)
    {
        $this->employeeId = $employeeId;
        $this->activatedAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getActivatedAt(): \DateTimeImmutable
    {
        return $this->activatedAt;
    }
}
