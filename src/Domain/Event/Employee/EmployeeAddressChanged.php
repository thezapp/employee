<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Address;
use Employee\Domain\Model\Id;

class EmployeeAddressChanged implements EventInterface
{
    private Id $employeeId;
    private Address $oldAddress;
    private Address $newAddress;
    private \DateTimeImmutable $changedAt;

    public function __construct(Id $employeeId, Address $oldAddress, Address $newAddress)
    {
        $this->employeeId = $employeeId;
        $this->oldAddress = $oldAddress;
        $this->newAddress = $newAddress;
        $this->changedAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getOldAddress(): Address
    {
        return $this->oldAddress;
    }

    public function getNewAddress(): Address
    {
        return $this->newAddress;
    }

    public function getChangedAt(): \DateTimeImmutable
    {
        return $this->changedAt;
    }
}
