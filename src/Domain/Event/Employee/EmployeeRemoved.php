<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Id;

class EmployeeRemoved implements EventInterface
{
    private Id $employeeId;
    private \DateTimeImmutable $removedAt;

    public function __construct(Id $employeeId)
    {
        $this->employeeId = $employeeId;
        $this->removedAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getRemovedAt(): \DateTimeImmutable
    {
        return $this->removedAt;
    }
}
