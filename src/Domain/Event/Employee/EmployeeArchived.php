<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Id;

class EmployeeArchived implements EventInterface
{
    private Id $employeeId;
    private \DateTimeImmutable $archivedAt;

    public function __construct(Id $employeeId)
    {
        $this->employeeId = $employeeId;
        $this->archivedAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getArchivedAt(): \DateTimeImmutable
    {
        return $this->archivedAt;
    }
}
