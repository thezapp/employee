<?php

declare(strict_types=1);

namespace Employee\Domain\Event\Employee;

use Employee\Domain\Component\Dispatcher\EventInterface;
use Employee\Domain\Model\Id;
use Employee\Domain\Model\Phone;

class EmployeePhoneAdded implements EventInterface
{
    private Id $employeeId;
    private Phone $phone;
    private \DateTimeImmutable $addedAt;

    public function __construct(Id $employeeId, Phone $phone)
    {
        $this->employeeId = $employeeId;
        $this->phone = $phone;
        $this->addedAt = new \DateTimeImmutable();
    }

    public function getEmployeeId(): Id
    {
        return $this->employeeId;
    }

    public function getPhone(): Phone
    {
        return $this->phone;
    }

    public function getAddedAt(): \DateTimeImmutable
    {
        return $this->addedAt;
    }
}
