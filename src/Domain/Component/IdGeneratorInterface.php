<?php

declare(strict_types=1);

namespace Employee\Domain\Component;

interface IdGeneratorInterface
{
    public function generate(): string;
}
