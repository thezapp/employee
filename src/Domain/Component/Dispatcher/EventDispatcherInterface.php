<?php

declare(strict_types=1);

namespace Employee\Domain\Component\Dispatcher;

interface EventDispatcherInterface
{
    /**
     * @param EventInterface $event
     */
    public function dispatchEvents(array $events): void;

    public function dispatchEvent(EventInterface $event): void;
}
