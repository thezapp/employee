<?php

declare(strict_types=1);

namespace Employee\Infrastructure\Repository;

use Employee\Domain\Exception\EmployeeNotFoundException;
use Employee\Domain\Model\Employee;
use Employee\Domain\Model\EmployeeRepositoryInterface;
use Employee\Domain\Model\Id;

class EmployeeRepositoryInMemory implements EmployeeRepositoryInterface
{
    protected array $storage = [];

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        return $this->storage;
    }

    /**
     * @inheritDoc
     */
    public function getOne(Id $employeeId): ?Employee
    {
        $employee = $this->storage[$employeeId->toString()] ?? null;

        if ($employee === null) {
            throw new EmployeeNotFoundException($employeeId);
        }

        return $employee;
    }

    public function add(Employee $employee): void
    {
        $this->storage[$employee->getId()->toString()] = $employee;
    }

    public function update(Employee $employee): void
    {
        $this->storage[$employee->getId()->toString()] = $employee;
    }

    public function remove(Employee $employee): void
    {
        unset($this->storage[$employee->getId()->toString()]);
    }
}
