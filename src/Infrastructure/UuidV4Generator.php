<?php

declare(strict_types=1);

namespace Employee\Infrastructure;

use Employee\Domain\Component\IdGeneratorInterface;

class UuidV4Generator implements IdGeneratorInterface
{
    public function generate(): string
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff), // 16 bits for "time_mid"
            mt_rand(0, 0x0fff) | 0x4000, // 16 bits for "time_hi_and_version"
            mt_rand(0, 0x3fff) | 0x8000, // 16 bits, 8 bits for "clk_seq_hi_res", 8 bits for "clk_seq_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff) // 48 bits for "node"
        );
    }
}
