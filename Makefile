install:
	composer install

validate:
	composer validate

lint:
	composer run-script phpcs -- --standard=PSR12 src tests

test:
	composer run-script phpunit --no-cache